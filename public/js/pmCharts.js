$(function () {
    setViewPortOrientation();
    $("body").click(toggleFullScreen);
    google.charts.load('current', {'packages': ['gauge']});
    google.charts.setOnLoadCallback(drawChart);
});

function drawChart() {
    const outsideGaugeFactor = 3;
    var gaugeSize;

    $(window).resize((function () {
        var timeout;
        return function () {
            clearTimeout(timeout);
            timeout = setTimeout(function () {
                adjustToWindowSize();

                chart25.draw(data25, options25);
                chart10.draw(data10, options10);
                outsideChart25.draw(outsideData25, outsideOptions25);
                outsideChart10.draw(outsideData10, outsideOptions100);

                setViewPortOrientation();
            }, 100);
        }
    })());

    var data25 = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['PM2.5', 0]
    ]);

    var data10 = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['PM10', 0]
    ]);

    var outsideData25 = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['outside', 0]
    ]);

    var outsideData10 = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['outside', 0]
    ]);

    var animationParams = {
        easing  : "linear",
        duration: disableAnimations ? 0 : pollingInterval * 1000
    };

    var initialAnimationParams = {
        easing  : "out",
        duration: disableAnimations ? 0 : 950
    };

    var options25 = {
        greenFrom : 0,
        greenTo   : 20,
        yellowFrom: 30,
        yellowTo  : 75,
        redFrom   : 75,
        redTo     : 200,
        minorTicks: 5,
        min       : 0,
        max       : 200,
        animation : initialAnimationParams
    };
    var options10 = {
        greenFrom : 0,
        greenTo   : 50,
        yellowFrom: 70,
        yellowTo  : 100,
        redFrom   : 100,
        redTo     : 400,
        minorTicks: 5,
        min       : 0,
        max       : 400,
        animation : initialAnimationParams
    };

    var outsideOptions25  = $.extend({}, options25);
    var outsideOptions100 = $.extend({}, options10);

    adjustToWindowSize();

    var chart25        = new google.visualization.Gauge(document.getElementById('gauge25')),
        chart10        = new google.visualization.Gauge(document.getElementById('gauge10')),
        outsideChart25 = new google.visualization.Gauge(document.getElementById('outside25')),
        outsideChart10 = new google.visualization.Gauge(document.getElementById('outside10'));

    chart25.draw(data25, options25);
    chart10.draw(data10, options10);
    outsideChart25.draw(outsideData25, outsideOptions25);
    outsideChart10.draw(outsideData10, outsideOptions100);


    setTimeout(updateGraphs, 0);

    setTimeout(function () {
        options10.animation = animationParams;
        options25.animation = animationParams;
    }, 950);

    setInterval(updateGraphs, pollingInterval * 1000);

    var previousValues = {
        pm100: null,
        pm25 : null
    };

    var prevOutsideValues = {
        pm10: null,
        pm23: null
    };

    function adjustToWindowSize() {
        gaugeSize = calculateGaugeSize();
        setContainersSize(gaugeSize);
        options10.width          = gaugeSize;
        options10.height         = gaugeSize;
        options25.width          = gaugeSize;
        options25.height         = gaugeSize;
        outsideOptions25.width   = gaugeSize / outsideGaugeFactor;
        outsideOptions25.height  = gaugeSize / outsideGaugeFactor;
        outsideOptions100.width  = gaugeSize / outsideGaugeFactor;
        outsideOptions100.height = gaugeSize / outsideGaugeFactor;
    }

    var messages = (function () {
        var messagesInterval;
        var $messagesContainer = $("#messagesContainer");

        var colors  = ["#000000", "#ffffff"],
            options = {
                color      : colors[0],
                trailColor : colors[1],
                duration   : pollingInterval * 1000,
                strokeWidth: 3,
                trailWidth : 3,
            };

        var circle,
            cycle = 0;

        return {
            countdownTo  : function (timestamp, interval) {
                interval = interval || 1000;

                if(circle) {
                    circle.destroy();
                }

                options.color      = colors[1 - cycle];
                options.trailColor = colors[cycle];
                options.duration   = timestamp - (new Date()).getTime();

                circle = new ProgressBar.Circle('#progressBarContainer', options);
                cycle  = (cycle + 1) % 2;

                clearInterval(messagesInterval);
                setTimeout(displayMessage, interval / 10);

                messagesInterval = setInterval(displayMessage, interval);
                function displayMessage() {
                    var timeLeft = Math.max(0, ((timestamp - new Date()) / 1000));
                    $messagesContainer
                        .text("updating readings in " + Math.ceil(timeLeft) + " seconds")
                        .removeClass("error");
                }

                circle.set(1);
                circle.animate(0);

            },
            countdownStop: function () {
                clearInterval(messagesInterval);
            }
        }
    })();

    function updateGraphs() {
        var nextUpdateTimestamp = (new Date((new Date()).getTime() + pollingInterval * 1000));
        messages.countdownStop();
        var updatingMessageTimeout = setTimeout(function () {
            $("#messagesContainer")
                .text("updating...")
                .removeClass("error");
        }, 100);

        $.getJSON("/getData.json", function (data) {
            clearTimeout(updatingMessageTimeout);
            if (pageLoadedTimestamp && data.metadata.instanceStart > pageLoadedTimestamp) {
                location.reload();
            }
            const insideData  = data.data,
                  outsideData = data.dataAirly;

            if (insideData.pm25 != previousValues.pm25) {
                data25.setValue(0, 1, insideData.pm25);
                chart25.draw(data25, options25);
                previousValues.pm25 = insideData.pm25;
            }

            if (insideData.pm100 != previousValues.pm100) {
                data10.setValue(0, 1, insideData.pm100);
                chart10.draw(data10, options10);
                previousValues.pm100 = insideData.pm100;
            }

            if (outsideData.pm25 != prevOutsideValues.pm25) {
                outsideData25.setValue(0, 1, outsideData.pm25);
                outsideChart25.draw(outsideData25, outsideOptions25);
                prevOutsideValues.pm25 = outsideData.pm25;
            }

            if (outsideData.pm10 != prevOutsideValues.pm10) {
                outsideData10.setValue(0, 1, outsideData.pm10);
                outsideChart10.draw(outsideData10, outsideOptions100);
                prevOutsideValues.pm10 = outsideData.pm10;
            }

            messages.countdownTo(nextUpdateTimestamp);
        }).fail(function () {
            $("#messagesContainer")
                .text("error getting data")
                .addClass("error");
        });
    }

    function setContainersSize(gaugeSize) {
        if (viewPortIsHorizontal()) {
            $('#gaugesContainer')
                .width(gaugeSize * 2)
                .height(gaugeSize);
        } else {
            $('#gaugesContainer')
                .width(gaugeSize)
                .height(gaugeSize * 2);
        }

        $("#progressBarContainer")
            .width(gaugeSize / outsideGaugeFactor / 2)
            .height(gaugeSize / outsideGaugeFactor / 2);

        $(".gaugeContainer")
            .width(gaugeSize)
            .height(gaugeSize);

        $(".gasMask")
            .width(gaugeSize / 8)
            .height(gaugeSize / 8);

        $(".outsideGauge")
            .width(gaugeSize / outsideGaugeFactor)
            .height(gaugeSize / outsideGaugeFactor);
    }

    function calculateGaugeSize() {
        var gaugeSize;
        if (viewPortIsHorizontal()) {
            gaugeSize = Math.min(getWindowSize().width / 2, getWindowSize().height);
        } else {
            gaugeSize = Math.min(getWindowSize().width, getWindowSize().height / 2);
        }
        return gaugeSize;
    }
}

function setViewPortOrientation() {
    if (viewPortIsHorizontal()) {
        $('body').addClass('horizontal').removeClass('vertical');
    } else {
        $('body').addClass('vertical').removeClass('horizontal');
    }
}

function viewPortIsHorizontal() {
    return getWindowSize().width > getWindowSize().height;
}

var getWindowSize = (function () {
    var docEl               = document.documentElement,
        IS_BODY_ACTING_ROOT = docEl && docEl.clientHeight === 0;

    // Used to feature test Opera returning wrong values
    // for documentElement.clientHeight.
    function isDocumentElementHeightOff() {
        var d   = document,
            div = d.createElement('div');

        div.style.height = "2500px";
        d.body.insertBefore(div, d.body.firstChild);
        var r = d.documentElement.clientHeight > 2400;
        d.body.removeChild(div);
        return r;
    }

    if (typeof document.clientWidth == "number") {
        return function () {
            return {width: document.clientWidth, height: document.clientHeight};
        };
    } else if (IS_BODY_ACTING_ROOT || isDocumentElementHeightOff()) {
        var b = document.body;
        return function () {
            return {width: b.clientWidth, height: b.clientHeight};
        };
    } else {
        return function () {
            return {width: docEl.clientWidth, height: docEl.clientHeight};
        };
    }
})();

function toggleFullScreen() {
    var doc   = window.document;
    var docEl = doc.documentElement;

    var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
    var cancelFullScreen  = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

    if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
        requestFullScreen.call(docEl);
    }
    else {
        cancelFullScreen.call(doc);
    }
}
