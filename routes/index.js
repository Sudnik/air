"use strict";
var express              = require('express'),
    async                = require('async'),
    debug                = require('debug')('air:router'),
    debugDatagetterAirly = require('debug')('air:datagetter:airly'),
    debugDatagetterFile  = require('debug')('air:datagetter:file'),
    debugDataPusher      = require('debug')('air:datapusher'),
    router               = express.Router(),
    request              = require('request'),
    bodyParser           = require('body-parser'),
    fs                   = require('fs'),
    expressCacheOnDemand = require('express-cache-on-demand'),
    redisCache           = require('../modules/cache'),
    logger               = require('../modules/consoleLogger').consoleLogger,
    errorLogger          = require('../modules/consoleLogger').consoleErrorLogger;

const config                 = require('../modules/config');
const instanceStartTimestamp = (new Date()).getTime();

debug('initializing');

router.use(bodyParser.json());

router.get('/',
    redisCache.route(),
    expressCacheOnDemand(),
    function (req, res, next) {
        debug('received http request for /');
        fs.readFile(req.app.locals.config.dataFileLocation, (err, fileContent) => {
            if (err) {
                return next(err);

            }
            res.render('index', {
                pollingInterval  : parseInt(config.pollingInterval),
                disableAnimations: config.disableAnimations
            });
        });

    });

router.post('/', (req, res, next) => {
    debug('received http POST request');
    if (req.body && req.body.timestamp && req.body.pm100) {
        debug('writing data to file ' + config.dataFileLocation);
        fs.writeFile(config.dataFileLocation, JSON.stringify(req.body, null, 2), (err) => {
            if (err) {
                debug('error writing data to file ' + config.dataFileLocation, err);
            } else {
                debug('writen data to file ' + config.dataFileLocation);
            }
            res.send({message: "received data"});
        });
    } else {
        res.status(400);
        res.send();
    }
});

router.get('/getData.json',
    expressCacheOnDemand(),
    (req, res, next) => {
        debug('received http request for /getData.json');
        async.parallel({
            local: dataGetter.getValues,
            airly: airlyDataGetter.getValues
        }, (err, results) => {
            if (err) {
                debug('error assembling data to send response: ' + err);
                res.status(500);
                return res.send(err);
            }
            debug('sending json data to the browser');
            res.send({
                metadata : {
                    instanceStart: instanceStartTimestamp
                },
                data     : results.local,
                dataAirly: results.airly
            });

            if(req.app.locals.shutdown) {
                res.on('finish', () => {
                    debug('destroying socket after sending response to the client');
                    req.socket.destroy();
                });
            }
        });
    });

module.exports = router;

let airlyDataGetter = (() => {

    let values;

    getData();

    setInterval(getData, 15 * 60 * 1000);

    debugDatagetterAirly('fetching remote data with interval of ' + 15 + ' minutes');

    function getData(cb) {
        request(
            {
                url    : "https://airapi.airly.eu/v1//mapPoint/measurements?latitude=50.08733&longitude=19.95044000000007&historyHours=24&historyResoltionHours=1&apikey=fae55480ef384880871f8b40e77bbef9",
                headers: {
                    "User-Agent"     : "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
                    "Accept"         : "*/*",
                    "Accept-Language": "en-US,en;q=0.8,pl;q=0.6,zh;q=0.4",
                    "Connection"     : "keep-alive",
                    "Orign"          : "https://map.airly.eu/",
                    "Referer"        : "https://map.airly.eu/"
                }
            },
            (err, response, body) => {
                if (err) {
                    debugDatagetterAirly(err);
                    values = {};
                    return;
                }
                let data = {};

                try {
                    data = JSON.parse(body);
                } catch (e) {
                    debugDatagetterAirly('problem parsing json data ' + e);
                    return;
                }

                debugDatagetterAirly('fetched json data from remote location');

                if(!data.currentMeasurements) {
                    debugDatagetterAirly('no currentMeasurements');
                    return;
                }

                values           = {
                    humidity   : Math.round(data.currentMeasurements.humidity),
                    pm25       : Math.round(data.currentMeasurements.pm25),
                    pm10       : Math.round(data.currentMeasurements.pm10),
                    pressure   : Math.round(data.currentMeasurements.pressure),
                    temperature: Math.round(data.currentMeasurements.temperature * 10) / 10,
                };
                values.timestamp = (new Date()).getTime();
            }
        )
    }

    return {
        getValues: (cb) => {
            cb(null, values);
        }
    }
})();

var dataGetter = (() => {
    var lastData,
        currentDataSet = [],
        averages       = {},
        error          = null;

    readFile();

    setInterval(readFile, 1000);

    debugDatagetterFile('updating data from file with interval of 1s');

    return {
        getValues: (cb) => {
            if (error) {
                cb(error);
            } else {
                cb(null, averages);
            }
        }
    };

    function readFile() {
        fs.readFile(config.dataFileLocation, (err, fileContent) => {
            const numberOfSteps = config.steppingAverageSteps;
            if (err) {
                debugDatagetterFile('problem getting data from file ' + config.dataFileLocation);
                error = err;
                return;
            }
            try {
                lastData = JSON.parse(fileContent)
            } catch (e) {
                error = e;
                debugDatagetterFile('error in data file: ' + error);
                return;
            }

            debugDatagetterFile('got data from file, timestamp: ' + lastData.timestamp);

            dataPusher.pushData(lastData);

            error = null;
            currentDataSet.push(lastData);
            if (currentDataSet.length > numberOfSteps) {
                currentDataSet.splice(0, 1);
            }

            var sums = currentDataSet.reduce((p, c) => {
                var result = {};
                for (var key in c) {
                    result[key] = p[key] + c[key];
                }
                return result;
            });

            averages = {};

            for (var key in sums) {
                averages[key] = (sums[key] / currentDataSet.length).toFixed(config.averagesAccuracy);
            }

            averages.timestamp = (new Date()).getTime() / 1000;

        });
    }

})();

const dataPusher = (() => {
    const pushDataTo = typeof config.pushDataTo === 'string' ? [config.pushDataTo] : config.pushDataTo;

    const push = (() => {
        const initialErrorDelay = 1000,
              maxErrorDelay     = 60000;
        let endpoints = {};
        return (data, url, cb) => {
            if (!endpoints[url] || !endpoints[url].waitingForPushEnd) {
                if (!endpoints[url]) {
                    endpoints[url] = {
                        errorDelay: initialErrorDelay
                    };
                }
                endpoints[url].waitingForPushEnd = true;
                debugDataPusher("[" + url + "] " + "pushing");
                request.post({
                    url    : url,
                    headers: {
                        "Content-Type": "application/json"
                    },
                    forever: true,
                    body   : JSON.stringify(data)
                }, (err, response) => {
                    if (err || (response && response.statusCode !== 200)) {
                        setTimeout(() => {
                            endpoints[url].waitingForPushEnd = false;
                            endpoints[url].errorDelay        = Math.min(endpoints[url].errorDelay * 2, maxErrorDelay);
                            cb(err);
                        }, endpoints[url].errorDelay);
                        debugDataPusher("[" + url + "] " + (err || "bad response status code: " + response.statusCode));
                        debugDataPusher("[" + url + "] " + "next attempt in " + endpoints[url].errorDelay / 1000 + ' seconds');
                        return;
                    }
                    debugDataPusher("[" + url + "] " + "pushed");
                    endpoints[url].errorDelay        = initialErrorDelay;
                    endpoints[url].waitingForPushEnd = false;
                    return cb();
                })
            } else {
                debugDataPusher("[" + url + "] " + "skipping push");
                cb();
            }
        }
    })();

    return {
        pushData(data, cb = () => {}){
            if (pushDataTo && pushDataTo.length) {
                async.each(pushDataTo, (url, cb) => {
                    push(data, url, cb);
                });
            }
            cb();
        }
    }
})();