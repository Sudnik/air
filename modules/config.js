"use strict";

const fs         = require('fs'),
      _          = require('underscore'),
      configFile = '../config/config.json',
      debug      = require('debug')('air:config');

debug("initializing");

let config = {};

try {
    config = require(configFile);
    debug('loaded configuration file: ' + configFile);
} catch (e) {
    if (e.code === 'MODULE_NOT_FOUND') {
        debug('no configuration file ' + configFile + ', using default configuration');
    } else {
        debug("unknown error: ", e);
        throw e;
    }
}

const configDefaults = JSON.parse(fs.readFileSync('./config/config.json.sample'));

config = _.defaults(config, configDefaults);
config.webServerParams = _.defaults(config.webServerParams, configDefaults.webServerParams);

module.exports = config;



