var errorLogger = require('../modules/consoleLogger').consoleErrorLogger;
var redisCacheConfig,
    debug  = require('debug')('air:cache'),
    config = require('./config');

if (config.disableExpressRedisCache) {
    debug('cache disabled in config file, mocking the interface');
    module.exports = {
        route: () => (req, res, next) => next()
    };
} else {
    try {
        redisCacheConfig = require('../config/cacheParams.json');
    } catch (e) {
        console.log('[' + (new Date()).toUTCString() + '] [cache] no redisCacheConfig file, using defaults');
        redisCacheConfig = {expires: 10};
    }
    module.exports = require('express-redis-cache')(redisCacheConfig)
        .on('error', function (e) {
            errorLogger(e.toString());
        });
}