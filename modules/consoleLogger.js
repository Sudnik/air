"use strict";
var moment = require('moment'),
    colors = require('colors');


module.exports.consoleLogger = function(message){
    console.log('[' + moment().format('YYYY-MM-DD HH:mm:ss.SSS') + '] ' + colors.green(message));
};

module.exports.consoleErrorLogger = function (message){
    console.log('[' + moment().format('YYYY-MM-DD HH:mm:ss.SSS') + '] ' + colors.red(message));
};
